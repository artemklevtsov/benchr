PACKAGE = $(shell grep '^Package: ' DESCRIPTION | awk '{ print $$2 }')
VERSION = $(shell grep '^Version: ' DESCRIPTION | awk '{ print $$2 }')
PARENTDIR = ..
TARBALL = $(PACKAGE)_$(VERSION).tar.gz
CHECKDIR = $(PARENTDIR)/$(PACKAGE).Rcheck
RSCRIPT = Rscript --vanilla
RCMD = R --vanilla --quiet

all: doc check install

deps:
	$(RSCRIPT) -e 'if (!require(devtools)) install.packages("devtools")'
	$(RSCRIPT) -e 'if (!require(roxygen2)) install.packages("roxygen2")'
	$(RSCRIPT) -e 'devtools::install_dev_deps()'

doc:
	$(RSCRIPT) -e 'devtools::document()'

readme: README.Rmd
	$(RSCRIPT) -e 'rmarkdown::render("$<", output_options = list(html_preview = FALSE))'

man: doc
	$(RCMD) CMD Rd2pdf --no-preview --force -o $(PACKAGE)_$(VERSION)-manual.pdf .

check: deps
	$(RSCRIPT) -e 'devtools::check(force_suggests = TRUE)'

test: deps
	$(RSCRIPT) -e 'devtools::test()'

install:
	$(RSCRIPT) -e 'devtools::install(build_vignettes = TRUE)'

build:
	$(RSCRIPT) -e 'devtools::build()'

build-win:
	$(RSCRIPT) -e 'devtools::build_win()'

vignettes: deps
	$(RSCRIPT) -e 'devtools::build_vignettes()'

submit:
	$(RSCRIPT) -e 'devtools::submit_cran()'

coverage: deps
	$(RSCRIPT) -e 'covr::package_coverage(type = "all")'

clean:
	rm -f $(PARENTDIR)/$(TARBALL)
	rm -rf $(CHECKDIR)
	rm -f src/*.o
	rm -f src/*.so
	rm -f src/*.dll
	rm -f vignettes/*.md
	rm -f vignettes/*.html
	rm -f vignettes/*.pdf
	rm -f README.html
	find . -name '.Rhistory' -delete

.PHONY: build doc data install test check vignettes
